# Copyright (c) 2020 Benjamin Böhme  <mail[at]benjamin-boehme.net>
# Licensed under the MIT License; see the file LICENSE for details
""" Tools for renaming files """
import pathlib

def rename_file(old_absolute_path, new_absolute_path):
    """ Warning: overwrites existing files silently! """
    old = pathlib.Path(old_absolute_path)
    new = pathlib.Path(new_absolute_path)
    old.rename(new)

def rename_files(rename_view):
    """
    Given a view
        ("old absolute file path", "new absolute file path"),
    rename all those files.
    """
    for old_absolute_path, new_absolute_path in rename_view:
        if old_absolute_path != new_absolute_path:
            rename_file(old_absolute_path, new_absolute_path)