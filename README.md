## FileTools v0.1
Some convenient tools for renaming and reorganizing file hierarchies, licensed
under the MIT license.

Copyright (C) 2020  Benjamin Böhme <mail[at]benjamin-boehme.net>

**Warning:**
- Careless use of these tools can lead to data loss!
- Use at your own risk!
- Backup your data before use!
- Tested on a UNIX system - be careful on a Windows file system!

### Usage
Run

	python3 filetools.py <command> [<options>]

where &lt;command> is one of the commands

    help
    extend-file-names
    flatten
    purge

and [&lt;options>] is a list of command-specific options described below.

### help
Usage:

	python3 filetools.py help

Display available commands.

### extend-file-names
Usage:

	python3 filetools.py extend-file-names <path> [<options>]

Change all the names of files "in scope" (see below) under the &lt;path> by
prepending a prefix and appending a suffix to the file name, i.e. the file

> &lt;path>/relative/path/to/filename.filextension

will be renamed to

> &lt;path>/relative/path/to/&lt;prefix>filename&lt;suffix>.filextension

The prefix and suffix may contain certain patterns that will be resolved to
directory names, see below.

The program will list all the files to be renamed and ask for confirmation
before proceeding.

Options:

	-h
	--help
Show help text

	-p <prefix>
	--prefix <prefix>
The prefix

	-s <suffix>
	--suffix <suffix>
The suffix

	--scope <scope>
Specify the scope of files under &lt;path> to be considered for renaming.
The value &lt;scope> must be one of
- *all*: Include all files under &lt;path>. This is the default.
- *bottom*: Include only files under &lt;path> that are at the bottom of the file
hierarchy, i.e. that are contained inside directories that do not themselves
have subdirectories.

#### Patterns
The strings &lt;prefix> and &lt;prefix> may contain one of the following patterns:

	%d
This will be expanded to the name of the directory containing the respective
file.

	%D
This will be expanded to the following string:  
The names of all the directories in the relative path of the file (i.e.,
relative to &lt;path>), separated by ' - '.  
For example, if there is a file

> &lt;path>/a/b/c/d.ex

then running

	python3 filetools.py extend-file-names <path> --prefix %D_

would cause that file to be renamed to

> &lt;path>/a/b/c/a - b - c_d.ex

### purge
Usage:

	python3 filetools.py purge <path> [<options>]

Perform a post-order walk on the file tree under &lt;path> and remove all empty
directories (i.e. empty child directories are remove before their parents are
considered).

The program will list all the directories to be removed and ask for confirmation
before proceeding.

Options:

	-h
	--help
Show help text

	-y
	--yes
Do not list the directories; do not ask for confirmation.

### flatten
Usage:

	python3 filetools.py flatten <path> [<options>]

Flatten the file tree under &lt;path>, i.e. move all files located in nested
subdirectories of &lt;path> to &lt;path> and then purge all empty directories below
&lt;path>.

To avoid data loss, the program will abort without moving any files at all in
case there are file name collisions (i.e. files of the same located in distinct
subdirectories of &lt;path>).

After verifying that there are no name collisions, the program will ask for
confirmation before moving files and purging.

Options:

	-h
	--help
Show help text

	-y
	--yes
Do not ask for confirmation.
