# Copyright (c) 2020 Benjamin Böhme  <mail[at]benjamin-boehme.net>
# Licensed under the MIT License; see the file LICENSE for details
import argparse
import iotools
import os
import purge
import query
import rename

def flatten(arg_list):
    args = _parse_arguments(arg_list)
    file_list = query.list_all_file_paths_in_scope(args.path, 'all')
    collision_dict = _get_collision_dict(file_list)
    if _collisions_exist(collision_dict):
        _print_collisions(collision_dict)
        print('\nAborting program. Resolve the collisions and try again!')
        exit()
    permission_to_proceed = args.yes
    if not permission_to_proceed:
        prompt_string = '''There are no file name collisions.
    Proceed to flatten the file tree under {}?'''.format(args.path)
        permission_to_proceed = iotools.ask_y_or_n(prompt_string)
    if permission_to_proceed:
        rename_view = _get_rename_view(args.path, file_list)
        rename.rename_files(rename_view)
        purge.purge([args.path, '--yes'])

def _parse_arguments(arg_list):
    parser = argparse.ArgumentParser(
        prog = 'FileTools flatten',
        description = program_description)
    parser.add_argument(
        'path',
        help='target directory for file operations')
    parser.add_argument(
        '-y',
        '--yes',
        action='store_true',
        help='do not ask for confirmation; run silently unless there are collisions')
    return parser.parse_args(arg_list)

def _get_collision_dict(file_list):
    """
    From list of file paths, create dict of the form
        <file name> -> <path list>
    where <file name> is a file name (including the file extension, but
    excluding the containing directories), and <path list> is a list of all
    directory paths <d> such that file_list contains the path <d>/<file name>
    """
    dict = {}
    for file_path in file_list:
        file_dir, file_name = os.path.split(file_path)
        try:
            path_list = dict[file_name]
        except KeyError:
            dict[file_name] = [file_dir]
        else:
            path_list.append(file_dir)
    return dict

def _collisions_exist(collision_dict):
    for file_name, dir_list in collision_dict.items():
        if len(dir_list) > 1:
            return True
    return False

def _print_collisions(collision_dict):
    for file_name, dir_list in collision_dict.items():
        if len(dir_list) > 1:
            print('File name collision: {} found at more than one location: {}\n'.format(
                file_name, dir_list))

def _get_rename_view(base_path, file_list):
    """
    Create view (i.e. list of 2-tuples) of the form
        (<file path>, <flattened file path>)
    for all <file path> entries of file_list. Here, <flattened file path> is the
    path base_path<file name>, where <file name> is the name (incl. file
    extension) of the file <file path> without the containing directories.
    """
    rename_view = []
    for file_path in file_list:
        flattened_path = _get_flattened_path(base_path, file_path)
        pair = (file_path, flattened_path)
        rename_view.append(pair)
    return rename_view

def _get_flattened_path(base_path, file_path):
    if not base_path in file_path:
        raise ValueError('Base path must be contained in file path!')
    file_dir, file_name = os.path.split(file_path)
    flattened_path = os.path.join(base_path, file_name)
    return flattened_path

program_description = '''
Flatten a file tree (i.e. move all files to the top level and purge all empty
directories)
'''
