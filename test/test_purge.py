# Copyright (c) 2020 Benjamin Böhme  <mail[at]benjamin-boehme.net>
# Licensed under the MIT License; see the file LICENSE for details
"""
Warning:
  1) This code is Unix-specific and assumes the existence of a directory /tmp at
  the top of the file hierarchy.
  2) The tests will fail if /tmp/purgetestdir already exists. This is
  intended to avoid accidental compromising of data.
"""
import os
import pathlib
import purge
from unittest import TestCase

class PurgeTestCase(TestCase):
    """ Tests for module 'purge' """

    def setUp(self):
        self._setup_dirs()
        self._setup_files()

    def _setup_dirs(self):
        self.test_dir = pathlib.Path('/tmp/purgetestdir')
        self.x = self.test_dir / 'x'
        self.xx = self.x / 'xx'
        self.xy = self.x / 'xy'
        self.y = self.test_dir / 'y'
        self.yx = self.y / 'yx'
        self.yy = self.y / 'yy'
        self.z = self.test_dir / 'z'
        self.zx = self.z / 'zx'
        for dir in [self.test_dir, self.x, self.xx, self.xy, self.y, self.yx, self.yy, self.z, self.zx]:
            dir.mkdir() # fails if test_dir already exists

    def _setup_files(self):
        self.files = []
        self.files.append(self.x / 'x.file')
        self.files.append(self.xx / 'xx.file')
        self.files.append(self.yx / 'yx.file')
        for f in self.files:
            f.touch()

    def tearDown(self):
        os.system('rm -r {}'.format(str(self.test_dir)))

    def test_purge(self):
        purge.purge([str(self.test_dir), '--yes'])
        self._verify_purged_filetree()

    def test_purge_directories(self):
        empty_dir_list = [self.xy, self.yy, self.zx, self.z]
        purge._purge_directories(empty_dir_list)
        self._verify_purged_filetree()

    def _verify_purged_filetree(self):
        self.assertTrue(self.x.exists())
        self.assertTrue(self.xx.exists())
        self.assertFalse(self.xy.exists())
        self.assertTrue(self.y.exists())
        self.assertTrue(self.yx.exists())
        self.assertFalse(self.yy.exists())
        self.assertFalse(self.z.exists())
        self.assertFalse(self.zx.exists())
