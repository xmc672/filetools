# Copyright (c) 2020 Benjamin Böhme  <mail[at]benjamin-boehme.net>
# Licensed under the MIT License; see the file LICENSE for details
"""
Warning:
  1) This code is Unix-specific and assumes the existence of a directory /tmp at
  the top of the file hierarchy.
  2) The tests will fail if /tmp/flattentestdir already exists. This is
  intended to avoid accidental compromising of data.
"""
import os
import pathlib
import flatten
from unittest import TestCase

class FlattenTestCase(TestCase):
    """ Tests for module 'flatten' """

    def test_flatten(self):
        self._setup_flatten_test()
        flatten.flatten(['/tmp/flattentestdir', '--yes'])
        self.assertTrue(self.test_dir.exists())
        self.assertTrue(self.topfile.exists())
        self.assertFalse(self.upper.exists())
        self.assertFalse(self.upperfile.exists())
        self.assertFalse(self.lower.exists())
        self.assertFalse(self.lowerfile.exists())
        self.assertFalse(self.other.exists())
        self.assertFalse(self.otherfile.exists())
        self.assertTrue((self.test_dir / 'upper.file').exists())
        self.assertTrue((self.test_dir / 'lower.file').exists())
        self.assertTrue((self.test_dir / 'other.file').exists())
        os.system('rm -r {}'.format(str(self.test_dir))) # clean up

    def _setup_flatten_test(self):
        self.test_dir = pathlib.Path('/tmp/flattentestdir')
        self.upper = self.test_dir / 'upper'
        self.lower = self.upper / 'lower'
        self.other = self.test_dir / 'other'
        for dir in [self.test_dir, self.upper, self.lower, self.other]:
            dir.mkdir() # fails if test_dir already exists
        self.topfile = self.test_dir / 'top.file'
        self.upperfile = self.upper / 'upper.file'
        self.lowerfile = self.lower / 'lower.file'
        self.otherfile = self.other / 'other.file'
        for f in [self.topfile, self.upperfile, self.lowerfile, self.otherfile]:
            f.touch()

    def test_get_collision_dict(self):
        self._setup_collision_test()
        file_list = [str(path_object) for path_object in self.files]
        actual_dict = flatten._get_collision_dict(file_list)
        expected_dict = {
            '1.coll': ['/tmp/flattentestdir', '/tmp/flattentestdir/x'],
            '2.coll': ['/tmp/flattentestdir', '/tmp/flattentestdir/x/xx'],
            'top.ok': ['/tmp/flattentestdir'],
            '1.ok': ['/tmp/flattentestdir/x'],
            'xy.coll': ['/tmp/flattentestdir/x', '/tmp/flattentestdir/y'],
            '2.ok': ['/tmp/flattentestdir/x/xx'],
            'y.ok': ['/tmp/flattentestdir/y']
            }
        self.assertEqual(actual_dict, expected_dict)
        os.system('rm -r {}'.format(str(self.test_dir))) # clean up

    def _setup_collision_test(self):
        self.test_dir = pathlib.Path('/tmp/flattentestdir')
        self.x = self.test_dir / 'x'
        self.xx = self.x / 'xx'
        self.y = self.test_dir / 'y'
        for dir in [self.test_dir, self.x, self.xx, self.y]:
            dir.mkdir() # fails if test_dir already exists
        self.files = []
        self.files.append(self.test_dir / '1.coll')
        self.files.append(self.test_dir / '2.coll')
        self.files.append(self.test_dir / 'top.ok')
        self.files.append(self.x / '1.coll')
        self.files.append(self.x / '1.ok')
        self.files.append(self.x / 'xy.coll')
        self.files.append(self.xx / '2.coll')
        self.files.append(self.xx / '2.ok')
        self.files.append(self.y / 'xy.coll')
        self.files.append(self.y / 'y.ok')
        for f in self.files:
            f.touch()

    def test_collisions_exist(self):
        nocoll_dict = {
            'name1': ['dir1'],
            'name2': ['dir2']
            }
        coll_dict = {
            'name1': ['dir1'],
            'name2': ['dir1', 'dir2']
            }
        self.assertFalse(flatten._collisions_exist(nocoll_dict))
        self.assertTrue(flatten._collisions_exist(coll_dict))

    def test_get_rename_view(self):
        base_path = '/tmp/flattentestdir'
        file_list = []
        file_list.append('/tmp/flattentestdir/top.file')
        file_list.append('/tmp/flattentestdir/upper/upper.file')
        file_list.append('/tmp/flattentestdir/upper/lower/lower.file')
        rename_view = flatten._get_rename_view(base_path, file_list)
        expected_view = [
            ('/tmp/flattentestdir/top.file', '/tmp/flattentestdir/top.file'),
            ('/tmp/flattentestdir/upper/upper.file', '/tmp/flattentestdir/upper.file'),
            ('/tmp/flattentestdir/upper/lower/lower.file', '/tmp/flattentestdir/lower.file')
            ]
        self.assertEqual(rename_view, expected_view)

    def test_get_flattened_path(self):
        base_path = '/tmp/flattentestdir'
        file_path = '/tmp/flattentestdir/some/path/to/file.test'
        expected_path = '/tmp/flattentestdir/file.test'
        flattened_path = flatten._get_flattened_path(base_path, file_path)
        self.assertEqual(flattened_path, expected_path)
