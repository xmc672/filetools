# Copyright (c) 2020 Benjamin Böhme  <mail[at]benjamin-boehme.net>
# Licensed under the MIT License; see the file LICENSE for details
import extend
from unittest import TestCase

class ExtendTestCase(TestCase):
    """ Tests for module 'extend' """

    def test_make_rename_dict(self):
        file_list = ['/somePath/someFile.test', '/other/path/and/file.test']
        actual_dict = extend._make_rename_dict(file_list, 'prefix_', '_suffix')
        expected_dict = {
            '/somePath/someFile.test': '/somePath/prefix_someFile_suffix.test',
            '/other/path/and/file.test': '/other/path/and/prefix_file_suffix.test'
            }
        self.assertEqual(actual_dict, expected_dict)

    def test_get_expanded_view(self):
        initial_view = [
            ('basepath/someFolder/someFile.test', 'basepath/someFolder/someFile.test'),
            ('basepath/two/three.test', 'basepath/two/one - %d - three.test'),
            ('basepath/a/b/c.test', 'basepath/a/b/%D - c.test')
            ]
        actual_view = extend._get_expanded_view(initial_view, 'basepath')
        expected_view = [
            ('basepath/someFolder/someFile.test', 'basepath/someFolder/someFile.test'),
            ('basepath/two/three.test', 'basepath/two/one - two - three.test'),
            ('basepath/a/b/c.test', 'basepath/a/b/a - b - c.test')
            ]
        self.assertEqual(actual_view, expected_view)
