# Copyright (c) 2020 Benjamin Böhme  <mail[at]benjamin-boehme.net>
# Licensed under the MIT License; see the file LICENSE for details
"""
Warning:
  1) This code is Unix-specific and assumes the existence of a directory /tmp at
  the top of the file hierarchy.
  2) The tests will fail if /tmp/renametestdir already exists. This is
  intended to avoid accidental compromising of data.
"""
import pathlib
import rename
from unittest import TestCase

class RenameTestCase(TestCase):
    """ Tests for module 'rename' """

    def setUp(self):
        self.test_dir = pathlib.Path('/tmp/renametestdir')
        self.test_dir.mkdir() # fails if test_dir already exists
        self.old_test_files = [
            self.test_dir / 'abc.no',
            self.test_dir / 'def.no']
        self.new_test_files = [
            self.test_dir / 'abc.yes',
            self.test_dir / 'def.yes']
        for f in self.old_test_files:
            f.touch()

    def tearDown(self):
        for f in self.old_test_files + self.new_test_files:
            try:
                f.unlink()
            except FileNotFoundError:
                pass
        self.test_dir.rmdir()

    def test_rename_file(self):
        old_path = self.old_test_files[0]
        new_path = self.new_test_files[0]
        rename.rename_file(str(old_path), str(new_path))
        self.assertFalse(old_path.exists())
        self.assertTrue(new_path.exists())

    def test_rename_files(self):
        zipped_test_files = zip(self.old_test_files, self.new_test_files)
        rename_view = [(str(old), str(new)) for (old, new) in zipped_test_files]
        rename.rename_files(rename_view)
        for (old, new) in zipped_test_files:
            self.assertFalse(old.exists())
            self.assertTrue(new.exists())
