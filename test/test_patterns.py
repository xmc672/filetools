# Copyright (c) 2020 Benjamin Böhme  <mail[at]benjamin-boehme.net>
# Licensed under the MIT License; see the file LICENSE for details
import patterns
from unittest import TestCase

class PatternsTestCase(TestCase):
    """ Tests for module 'patterns' """

    def test_expand_pattern_d(self):
        actual = patterns.expand_pattern_d('path/to/file.test')
        expected = 'path/to/file.test'
        self.assertEqual(actual, expected)
        actual = patterns.expand_pattern_d('path/two/one - %d - three.test')
        expected = 'path/two/one - two - three.test'
        self.assertEqual(actual, expected)

    def test_expand_pattern_D(self):
        actual = patterns.expand_pattern_D('/tmp/basepath', '/tmp/basepath/path/to/file.test')
        expected = '/tmp/basepath/path/to/file.test'
        self.assertEqual(actual, expected)
        actual = patterns.expand_pattern_D('/tmp/basepath', '/tmp/basepath/path/to/%D_file.test', separator=' - ')
        expected = '/tmp/basepath/path/to/path - to_file.test'
        self.assertEqual(actual, expected)
