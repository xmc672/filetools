# Copyright (c) 2020 Benjamin Böhme  <mail[at]benjamin-boehme.net>
# Licensed under the MIT License; see the file LICENSE for details
"""
Warning:
  1) This code is Unix-specific and assumes the existence of a directory /tmp at
  the top of the file hierarchy.
  2) The tests will fail if /tmp/querytestdir already exists. This is
  intended to avoid accidental compromising of data.
"""
import os
import pathlib
import query
from unittest import TestCase

class QueryTestCase(TestCase):
    """ Tests for module 'query' """

    def setUp(self):
        self.prefix = 'p'
        self.suffix = 's'
        self._setup_dirs()
        self._setup_files()

    def _setup_dirs(self):
        self.test_dir = pathlib.Path('/tmp/querytestdir')
        self.x = self.test_dir / 'x'
        self.xx = self.x / 'xx'
        self.xy = self.x / 'xy'
        self.y = self.test_dir / 'y'
        self.yx = self.y / 'yx'
        self.yy = self.y / 'yy'
        self.z = self.test_dir / 'z'
        self.zx = self.z / 'zx'
        for dir in [self.test_dir, self.x, self.xx, self.xy, self.y, self.yx, self.yy, self.z, self.zx]:
            dir.mkdir() # fails if test_dir already exists

    def _setup_files(self):
        self.files = []
        self.files.append(self.x / 'x.file')
        self.files.append(self.xx / 'xx.file')
        self.files.append(self.yx / 'yx.file')
        for f in self.files:
            f.touch()

    def tearDown(self):
        os.system('rm -r {}'.format(str(self.test_dir)))

    def test_list_all_file_paths_in_scope(self):
        complete_list = query.list_all_file_paths_in_scope(str(self.test_dir), 'all')
        bottom_list = query.list_all_file_paths_in_scope(str(self.test_dir), 'bottom')
        complete_expected = [
            '/tmp/querytestdir/x/x.file',
            '/tmp/querytestdir/x/xx/xx.file',
            '/tmp/querytestdir/y/yx/yx.file']
        bottom_expected = [
            '/tmp/querytestdir/x/xx/xx.file',
            '/tmp/querytestdir/y/yx/yx.file']
        self.assertEqual(set(complete_list), set(complete_expected))
        self.assertEqual(set(bottom_list), set(bottom_expected))

    def test_list_empty_dirs(self):
        actual = [str(path_object) for path_object
                  in query.list_empty_dirs('/tmp/querytestdir')]
        expected = [
            '/tmp/querytestdir/x/xy',
            '/tmp/querytestdir/y/yy',
            '/tmp/querytestdir/z/zx',
            '/tmp/querytestdir/z'
            ]
        self.assertEqual(set(actual), set(expected))
