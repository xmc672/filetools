# Copyright (c) 2020 Benjamin Böhme  <mail[at]benjamin-boehme.net>
# Licensed under the MIT License; see the file LICENSE for details
import argparse
import os
import rename
import patterns
import iotools
import query

def extend_file_names(arg_list):
    args = _parse_arguments(arg_list)
    _quit_if_prefix_and_suffix_empty(args.prefix, args.suffix)
    file_list = query.list_all_file_paths_in_scope(args.path, args.scope)
    rename_dict = _make_rename_dict(file_list, args.prefix, args.suffix)
    rename_view = list(rename_dict.items())
    rename_view.sort(key=lambda pair: len(pair[0]), reverse=True)
    rename_view = _get_expanded_view(rename_view, args.path)
    _print_rename_view(rename_view)
    if iotools.ask_y_or_n('Proceed?'):
        rename.rename_files(rename_view)

def _parse_arguments(arg_list):
    parser = argparse.ArgumentParser(
        prog = 'FileTools extend-file-names',
        description = program_description)
    parser.add_argument(
        'path',
        help='target directory for file operations')
    parser.add_argument('-p', '--prefix', default='')
    parser.add_argument('-s', '--suffix', default='')
    parser.add_argument(
        '--scope',
        default='all',
        choices=['all', 'bottom'],
        help='file scope')
    return parser.parse_args(arg_list)

def _quit_if_prefix_and_suffix_empty(prefix, suffix):
    if prefix == '' and suffix == '':
        print('Prefix and suffix are empty; nothing to do.')
        exit()

def _make_rename_dict(file_list, prefix, suffix):
    rename_dict = {}
    for file_path in file_list:
        file_dir, old_name_and_ending = os.path.split(file_path)
        old_name, ending = old_name_and_ending.split('.', maxsplit=1)
        new_name = prefix + old_name + suffix + '.' + ending
        rename_dict[file_path] = os.path.join(file_dir, new_name)
    return rename_dict

def _get_expanded_view(view, base_path):
    expanded_view = []
    for pair in view:
        old_path, new_path = pair
        expanded_new_path = patterns.expand_pattern_d(new_path)
        expanded_new_path = patterns.expand_pattern_D(base_path, expanded_new_path, separator=' - ')
        expanded_view.append((old_path, expanded_new_path))
    return expanded_view

def _print_rename_view(view):
    for old_path, new_path in view:
        print('Old: {}'.format(old_path))
        print('New: {}\n'.format(new_path))

program_description = '''
Prepend/append prefixes/suffixes to file names
'''
