# Copyright (c) 2020 Benjamin Böhme  <mail[at]benjamin-boehme.net>
# Licensed under the MIT License; see the file LICENSE for details

def ask_y_or_n(prompt):
    prompt = prompt + ' (y/n)'
    while(True):
        input_string = input(prompt)
        if input_string == 'y':
            return True
        elif input_string == 'n':
            return False
