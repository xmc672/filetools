# Copyright (c) 2020 Benjamin Böhme  <mail[at]benjamin-boehme.net>
# Licensed under the MIT License; see the file LICENSE for details
""" Tools for querying the file system
(i.e. listing certain files/directories) """
import os
import pathlib

def list_all_file_paths_in_scope(target_path, scope):
    """
    Return list of all file paths (relative to target_path) that are within the
    scope.
    'scope' must be one of:
        'all':    all files below the target_path
        'bottom': only files at the bottom of the directory tree, i.e. inside
                  directories that do not contain subdirectories
    """
    file_list = []
    walk = os.walk(target_path)
    for path, dirs, files in walk:
        if _is_in_scope(path, dirs, scope):
            _append_file_paths(file_list, path, files)
    return file_list

def _is_in_scope(path, dirs, scope):
    if scope == 'all':
        return True
    if scope == 'bottom':
        return len(dirs) == 0

def _append_file_paths(file_list, path, files):
    for file in files:
        abs_file_path = os.path.join(path, file)
        file_list.append(abs_file_path)

def list_empty_dirs(target_path):
    """
    Return list of all directories 'd' (relative to target_path) such that:
        - d does not contain any files (but possibly subdirectories)
        - none of the subdirectories of d contain files
    Child dirs will always be listed before their parents, so it is safe to use
    the resulting list for sequential deletion of folders.
    """
    empty_dir_list = []
    path = pathlib.Path(target_path)
    for dir in dirs(path):
        _find_empty_child_dirs(dir, empty_dir_list)
    return empty_dir_list

def dirs(path_object):
    """ Returns list of subdirectories """
    return [dir for dir in path_object.iterdir() if dir.is_dir()]

def files(path_object):
    """ Returns list of subdirectories """
    return [dir for dir in path_object.iterdir() if dir.is_file()]

def _find_empty_child_dirs(path_object, empty_dir_list):
    """
    Helper recursion. Return False if one of the following holds:
        - some subdir is not empty
        - the path itself is an empty dir
    """
    is_empty_downwards = True
    for dir in dirs(path_object):
        if not _find_empty_child_dirs(dir, empty_dir_list):
            is_empty_downwards = False
    if len(files(path_object)) > 0:
        is_empty_downwards = False
    if is_empty_downwards:
        empty_dir_list.append(path_object)
    return is_empty_downwards
