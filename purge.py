# Copyright (c) 2020 Benjamin Böhme  <mail[at]benjamin-boehme.net>
# Licensed under the MIT License; see the file LICENSE for details
import argparse
import iotools
import query

def purge(arg_list):
    args = _parse_arguments(arg_list)
    empty_dir_list = query.list_empty_dirs(args.path)
    permission_to_proceed = args.yes
    if not permission_to_proceed:
        print('The following directories do not contain files and can be purged:')
        _print_dir_list(empty_dir_list)
        permission_to_proceed = iotools.ask_y_or_n('Proceed?')
    if permission_to_proceed:
        _purge_directories(empty_dir_list)

def _parse_arguments(arg_list):
    parser = argparse.ArgumentParser(
        prog = 'FileTools purge',
        description = program_description)
    parser.add_argument(
        'path',
        help='target directory for file operations')
    parser.add_argument(
        '-y',
        '--yes',
        action='store_true',
        help='do not ask for confirmation; run silently')
    return parser.parse_args(arg_list)

def _print_dir_list(dir_list):
    for dir in dir_list:
        print(str(dir))

def _purge_directories(empty_dir_list):
    for dir in empty_dir_list:
        dir.rmdir()

program_description = '''
Purge empty sub-directories recursively
'''
