# Copyright (c) 2020 Benjamin Böhme  <mail[at]benjamin-boehme.net>
# Licensed under the MIT License; see the file LICENSE for details
import os

def expand_pattern_d(absolute_path):
    """
    Expand pattern '%d' to name of containing dir,
    e.g. 'some/path/%dx.y' -> 'some/path/pathx.y'
    """
    containing_dir = absolute_path.rsplit('/', maxsplit=2)[1]
    expanded_path = absolute_path.replace('%d', containing_dir)
    return expanded_path

def expand_pattern_D(base_path, absolute_path, separator=' - '):
    """
    Expand pattern '%D' to the path relative to base_path, separated by the
    string separator, e.g. if separator==' - ', then
        <base_path>/path/to/some.file'
    becomes
        '<base_path>/path/to/path - to - some.file'
    """
    if not base_path in absolute_path:
        raise ValueError('Base path must be contained in absolute path!')
    relative_path = os.path.relpath(absolute_path, start=base_path)
    components = relative_path.split('/')
    expanded_symbol = separator.join(components[:-1])
    expanded_path = absolute_path.replace('%D', expanded_symbol)
    return expanded_path
