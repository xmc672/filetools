# Copyright (c) 2020 Benjamin Böhme  <mail[at]benjamin-boehme.net>
# Licensed under the MIT License; see the file LICENSE for details
import argparse
import extend
import flatten
import purge

def main():
    args = _parse_arguments()
    commands[args.command](args.arg_list)

def _parse_arguments():
    parser = argparse.ArgumentParser(
        prog = 'FileTools',
        description = program_description,
        usage=usage_text,
        add_help=False)
    parser.add_argument(
        'command',
        help='the task to be executed',
        choices=list(commands))
    parser.add_argument(
        'arg_list',
        nargs=argparse.REMAINDER)
    args = parser.parse_args()
    return args

def _show_help(arg_list):
    print(help_text)
    for key in commands:
        print('    ' + key)

program_description = '''
Tools for manipulating file names and folder hierarchies: extend file names by
custom prefixes/suffixes; flatten folder hierarchies; purge empty directories
'''

usage_text = '''
FileTools <command> [<args>]
Run "FileTools help" for more information
'''

help_text = '''
Usage: FileTools <command> [<args>]
Run "FileTools <command> -h" for command-specific help.

Available commands:'''

commands = {
    'extend-file-names': extend.extend_file_names,
    'flatten': flatten.flatten,
    'help': _show_help,
    'purge': purge.purge
    }

# driver code
if __name__ == '__main__':
    main()
